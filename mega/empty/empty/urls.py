from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponse


def hi(response):
	return HttpResponse("HI from empty project")

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'empty.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^hi/', hi),
)
