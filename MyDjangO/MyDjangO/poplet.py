# import facebook
import json, ast
from django.db import models
from django.db.backends import *


# settings.py
from django.conf import settings

settings = settings(
		DATABASE_ENGINE = "django.db.backends.mysql",
        # DATABASE_NAME = "djangoDB",
        # DATABASE_USER = "root",
        # DATABASE_PASSWORD = "taslim",
        # DATABASE_HOST = "127.0.0.1'" 
        # DATABASE_PORT = "3306",

 
    # INSTALLED_APPS     = ("myApp")
)

class Post(models.Model):
    postId          =       models.CharField(max_length = 40)
    userId          =       models.CharField(max_length = 20)
    userName        =       models.CharField(max_length = 100)
    message         =       models.CharField(max_length = 1000)
    pictureURL      =       models.URLField()
    postURL         =       models.URLField()
    object_id       =       models.CharField(max_length = 20)
    created_time    =       models.DateTimeField()
    updated_time    =       models.DateTimeField()


    commentAfter    =       models.CharField(max_length = 100)
    commentBefore   =       models.CharField(max_length = 100)
    likeAfter       =       models.CharField(max_length = 100)
    likeBefore      =       models.CharField(max_length = 100)
    
    class Meta:
        app_label = 'appname'

    

class Application(models.Model):
    name            =       models.CharField(max_length = 40)
    namespace       =       models.CharField(max_length = 40)
    app_id          =       models.CharField(max_length = 40)
    Post            =       models.ForeignKey(Post)
    class Meta:
        app_label = 'appname'

class Likes(models.Model):
    userId          =       models.CharField(max_length = 20)
    userName        =       models.CharField(max_length = 100)
    Post            =       models.ForeignKey(Post)
    class Meta:
        app_label = 'appname'

class Comments(models.Model):
    commentId       =       models.CharField(max_length = 20)
    userId          =       models.CharField(max_length = 20)
    userName        =       models.CharField(max_length = 100)  
    message         =       models.CharField(max_length = 1000)
    created_time    =       models.DateTimeField()
    Post            =       models.ForeignKey(Post)
    class Meta:
        app_label = 'appname'

class Message_Tags(models.Model):
    tagId           =       models.CharField(max_length = 100)
    tagName         =       models.CharField(max_length = 100)  
    taggedIdType    =       models.CharField(max_length = 100)
    Post            =       models.ForeignKey(Post)
    class Meta:
        app_label = 'appname'

class Place(models.Model):
    PlaceId         =       models.CharField(max_length = 100)
    Name            =       models.CharField(max_length = 100)
    City            =       models.CharField(max_length = 100)
    Country         =       models.CharField(max_length = 100)
    Street          =       models.CharField(max_length = 100)
    Longitude       =       models.FloatField()
    Latitude        =       models.FloatField()
    Post            =       models.ForeignKey(Post)
    
    class Meta:
        app_label = 'appname'
 
def WriteFile(json):
	file = open("json2.txt", "w")
	file.write(json)
	file.close()

def ReadFile(filename):
	# file = open("foodbank.txt", "r")
	file = open(filename, "r")
	text = file.read()
	return jsonDump(text)

def jsonDump(object):
	return json.dumps(object, separators=(',',':'))

def AssignPost(post, data):
	post.postID = data["id"]

post = Post()
print post
print Post.objects.all()