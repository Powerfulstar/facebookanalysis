
import time
import urllib2
from urllib2 import urlopen
import re
import datetime
import json
import re
from bs4 import BeautifulSoup


def Read(filepath):
	file = open(filepath, 'r')
	text = file.read()
	# print text
	return text

 
def Write(data,path):
	file = open(path, 'w').write(data)
		

def CollectMainLinks(url):
	# sourceCode = Read("prothom-alo.html")
	sourceCode = urllib2.urlopen(url).read()
	soup = BeautifulSoup(sourceCode, from_encoding="sourceCode")			
	div = soup.html.body	
	content = soup.select('#main-menu ul .menu_color_ .dynamic')
	links = []
	import re
	# print content
	# print content
	for x in content:
		# print x
		links = re.findall(r'href="(.*?)"', str(x))
		for y in links:
			print y
			links.append(y)
	return (json.dumps(links))

todaysLimit = None
page = 1
AllSubPagesLink = []
CurrentLyVisitingLink = None
def CollectSubPageLinks(url):

	shouldVisitNexPage = True
	global todaysLimit 
	global page
	global AllSubPagesLink
	global CurrentLyVisitingLink

	# sourceCode = Read("bangladesh-article.html")
	sourceCode = urllib2.urlopen(url).read()
	soup = BeautifulSoup(sourceCode)

	content = soup.findAll("div", attrs={"class":"content_right"})

	for x in content:
		title = x.select('a')
		title = re.findall(r'<a href=".*?">(.*?)</a>', str(title))
		try:
			title = title[0]
		except Exception, e:
			print title			
		

		link = x.select('a')
		link = re.findall(r'<a href="(.*?)"', str(link))
		try:
			link = url + link[0]
		except Exception, e:
			print link			
		

		additionalInfo = x.select('.additional_info')
		additionalInfoSource = re.findall(r'<span .*?>(.*?)</span>', str(additionalInfo)) 
		try:
			additionalInfoSource = additionalInfoSource[0]
		except Exception, e:
			print additionalInfoSource			

		additionalInfoTime = re.findall(r'<span>(.*?)</span>', str(additionalInfo)) 
		try:
			additionalInfoTime = additionalInfoTime[0]
		except Exception, e:
			print additionalInfoTime			

		subStory = x.select('.content')
		subStory = re.findall(r'<a href=".*?">(.*?)</a>', str(subStory))		
		for x in subStory:
			subStory = x
			break

		if todaysLimit == None:
			todaysLimit = str(additionalInfoTime[15:])
		print title
		print link
		print additionalInfoSource
		print additionalInfoTime
		print subStory
		
		print "\n\n\n"
		
		if (additionalInfoTime!=None) & (additionalInfoTime[15:] != todaysLimit):
			shouldVisitNexPage = False
			page = 1
			break
		else:
			AllSubPagesLink.append(title)
	if shouldVisitNexPage:
		page += 1
		CollectSubPageLinks(CurrentLyVisitingLink+"?page="+str(page))

def Details(url):
	sourceCode = urllib2.urlopen(url).read()
	# sourceCode = Read("details.html")
	soup = BeautifulSoup(sourceCode)
	content = soup.h1
	print content
	content = soup.article
	content = re.findall(r'<p>(.*?)</p>', str(content))	
	for x in content:
		print x
	return None
# CollectMainLinks('http://www.prothom-alo.com/')

# CurrentLyVisitingLink = "http://www.prothom-alo.com/bangladesh/article/"
# CollectSubPageLinks(CurrentLyVisitingLink)
# Write(json.dumps(AllSubPagesLink),"newsTitle.txt")
# print todaysLimit

# Details("http://www.prothom-alo.com/bangladesh/article/466762/%E0%A6%9F%E0%A7%87%E0%A6%95%E0%A6%A8%E0%A6%BE%E0%A6%AB%E0%A7%87-%E0%A6%8F%E0%A6%95-%E0%A6%B2%E0%A6%BE%E0%A6%96-%E0%A6%87%E0%A6%AF%E0%A6%BC%E0%A6%BE%E0%A6%AC%E0%A6%BE-%E0%A6%89%E0%A6%A6%E0%A7%8D%E0%A6%A7%E0%A6%BE%E0%A6%B0")


# sourceCode = urllib2.urlopen("http://127.0.0.1:8000/rest/subpages").read()
# sourceCode = json.loads(sourceCode)
# for x in sourceCode:
# 	print x.encode('utf-8')
# print sourceCode.encode('utf-8')
url = "http://127.0.0.1:8000/rest/details?url=http://www.prothom-alo.com/bangladesh/article/467857/"
sourceCode = urllib2.urlopen(url).read()
sourceCode = json.loads(sourceCode)
for x in sourceCode:
	print x.encode('utf-8')