from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context, loader, RequestContext
from django.template import loader, Context
from django.shortcuts import render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt
from django import forms
import json
from django.http import HttpResponse
from django.core import serializers
import urllib2
from urllib2 import urlopen
import json
from bs4 import BeautifulSoup
import re
 

class News(object):
	def __init__(self):
		self.subStory = None
		self.additionalInfoTime = None
		self.additionalInfoSource = None
		self.link = None
		self.title = None

		class Meta:
			app_label = 'rest'
		
def FirstPage(request):
	url = "http://www.prothom-alo.com/"
	print url
	sourceCode = "halum"
	sourceCode = urllib2.urlopen(url).read()
	soup = BeautifulSoup(sourceCode, from_encoding="sourceCode")			
	div = soup.html.body	
	content = soup.select('#main-menu ul .menu_color_ .dynamic')
	links = re.findall(r'href="(.*?)"', str(content))
	return HttpResponse(json.dumps(links))	
 

 
# CurrentLyVisitingLink = "http://www.prothom-alo.com/bangladesh/article/"

 
def CollectSubPageLinks(request):

	if request.method == 'GET':
		print "true"
		CurrentLyVisitingLink = request.GET.get('url', '')
		print CurrentLyVisitingLink
	AllSubPagesLink = CollectSubPageLinksMethod(CurrentLyVisitingLink)

	NewsListJson = []
	NewsListJson += "["
	i = 0
	for x in AllSubPagesLink:
		x = json.dumps(vars(x))
		NewsListJson.append(x)
		i += 1
		if len(AllSubPagesLink) > i:
			NewsListJson+= ","
	NewsListJson += "]"
	return HttpResponse(NewsListJson)

def CollectSubPageLinksMethod(url):	
	AllSubPagesLink = []
	sourceCode = urllib2.urlopen(url).read()
	soup = BeautifulSoup(sourceCode)

	content = soup.findAll("div", attrs={"class":"content_right"})

	for x in content:

		news = News()
		title = x.select('a')
		title = re.findall(r'<a href=".*?">(.*?)</a>', str(title))
		try:
			title = title[0]
			news.title = title
		except Exception, e:
			print "Exception : " + str(e) + "\n"			
		

		link = x.select('a')
		link = re.findall(r'<a href="(.*?)"', str(link))
		try:
			link = url + link[0]
			shortLink = re.findall(r'^(.*/d?)', str(link))
			print "this is shortlink : " + str(shortLink)
			link = shortLink[0]
			news.link = link
		except Exception, e:
			print "Exception : " + str(e) + "\n"			
		

		additionalInfo = x.select('.additional_info')
		additionalInfoSource = re.findall(r'<span .*?>(.*?)</span>', str(additionalInfo)) 
		try:
			additionalInfoSource = additionalInfoSource[0]
			news.additionalInfoSource = additionalInfoSource
		except Exception, e:
			print "Exception : " + str(e) + "\n"			

		additionalInfoTime = re.findall(r'<span>(.*?)</span>', str(additionalInfo)) 
		try:
			additionalInfoTime = additionalInfoTime[0]
			news.additionalInfoTime = additionalInfoTime
		except Exception, e:
			print "Exception : " + str(e) + "\n"			

		subStory = x.select('.content')
		subStory = re.findall(r'<a href=".*?">(.*?)</a>', str(subStory))		
		for x in subStory:
			subStory = x
			news.subStory = subStory
			break
		# print news.title
		# if todaysLimit == None:
		# 	todaysLimit = str(additionalInfoTime[15:])
		# print title
		# print link
		# print additionalInfoSource
		# print additionalInfoTime
		# print subStory
		# print "\n\n\n"
		AllSubPagesLink.append(news)
	return AllSubPagesLink


def Details(request):

	if request.method == 'GET':
		print "true"
		CurrentLyVisitingLink = request.GET.get('url', '')
		print CurrentLyVisitingLink
	# url = "http://www.prothom-alo.com/bangladesh/article/466018/%E0%A6%9A%E0%A6%BE%E0%A6%81%E0%A6%AA%E0%A6%BE%E0%A6%87%E0%A6%A8%E0%A6%AC%E0%A6%BE%E0%A6%AC%E0%A6%97%E0%A6%9E%E0%A7%8D%E0%A6%9C%E0%A7%87-%E0%A6%A6%E0%A7%81%E0%A6%B0%E0%A7%8D%E0%A6%AC%E0%A7%83%E0%A6%A4%E0%A7%8D%E0%A6%A4%E0%A7%87%E0%A6%B0-%E0%A6%86%E0%A6%97%E0%A7%81%E0%A6%A8%E0%A7%87-%E0%A6%AA%E0%A7%81%E0%A7%9C%E0%A6%B2-%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A6%BE%E0%A6%95"
	sourceCode = urllib2.urlopen(CurrentLyVisitingLink).read()
	soup = BeautifulSoup(sourceCode)
	# content = soup.h1
	content = soup.article
	content = re.findall(r'<p>(.*?)</p>', str(content))	
	articleLines = []

	for x in content:
		articleLines.append(x)
	
	return HttpResponse(json.dumps(content))

def HelloFromViews(request):
	return HttpResponse("Hi")