from django.db import models

# Create your models here.
# class Poll(models.Model):
#     question = models.CharField(max_length=200)
#     pub_date = models.CharField(max_length=100'date published')

# class Choice(models.Model):
#     poll = models.ForeignKey(Poll)
#     choice_text = models.CharField(max_length=200)
#     votes = models.IntegerField(default=0)


class Post(models.Model):
	postId 			= 		models.CharField(max_length = 50)
	userId 			= 		models.CharField(max_length = 20)
	userName		= 		models.CharField(max_length = 100)
	message 		= 		models.CharField(max_length = 1000)
	pictureURL 		= 		models.URLField()
	postURL			= 		models.URLField()
	object_id 		= 		models.CharField(max_length = 20)
	created_time	= 		models.CharField(max_length=100)
	updated_time	= 		models.CharField(max_length=100)


	commentAfter	= 		models.CharField(max_length = 100)
	commentBefore	= 		models.CharField(max_length = 100)
	likeAfter		= 		models.CharField(max_length = 100)
	likeBefore		= 		models.CharField(max_length = 100)
    
	class Meta:
		app_label = 'DownloadJsonFromFacebook'

	

class Application(models.Model):
	name 			= 		models.CharField(max_length = 40)
	namespace		= 		models.CharField(max_length = 40)
	app_id			= 		models.CharField(max_length = 40)
	Post			=		models.ForeignKey(Post)
	class Meta:
		app_label = 'DownloadJsonFromFacebook'

class Likes(models.Model):
	userId			= 		models.CharField(max_length = 20)
	userName		= 		models.CharField(max_length = 100)
	Post			=		models.ForeignKey(Post)
	class Meta:
		app_label = 'DownloadJsonFromFacebook'

	def __unicode__(self):
		return u'%s %s %s' % (self.userId, self.userName, self.Post)

	def __str__(self):
		return u'%s %s %s' % (self.userId, self.userName, self.Post)

        

class Comments(models.Model):
	commentId		= 		models.CharField(max_length = 20)
	userId			= 		models.CharField(max_length = 20)
	userName		= 		models.CharField(max_length = 100)	
	message			= 		models.CharField(max_length = 1000)
	created_time	= 		models.CharField(max_length=100)
	Post			=		models.ForeignKey(Post)
	class Meta:
		app_label = 'DownloadJsonFromFacebook'

class Message_Tags(models.Model):
	tagId			= 		models.CharField(max_length = 100)
	tagName			= 		models.CharField(max_length = 100)	
	taggedIdType	=		models.CharField(max_length = 100)
	Post			=		models.ForeignKey(Post)
	class Meta:
		app_label = 'DownloadJsonFromFacebook'

class Place(models.Model):
	PlaceId			=		models.CharField(max_length = 100)
	Name			=		models.CharField(max_length = 100)
	City			=		models.CharField(max_length = 100)
	Country			=		models.CharField(max_length = 100)
	Street			=		models.CharField(max_length = 100)
	Longitude		=		models.FloatField()
	Latitude		=		models.FloatField()
	Post			=		models.ForeignKey(Post)
    
	class Meta:
		app_label = 'DownloadJsonFromFacebook'
 